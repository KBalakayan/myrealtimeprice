import React from 'react';
import SockJsClient from 'react-stomp';
import DataTable from 'react-data-table-component';
import MyButton from './Button.js' 
 

const columns = [
  
  {
    name: 'Product Name',
    selector: 'name',
    sortable: true,
  },
  {
    name: 'Symbol',
	cell: row => <div style={{ fontWeight:'bold' }}>{row.contract}:{row.exchange}</div>,
  }
  ,{
    name: 'Price',
    cell: row => <div>{((row.price.ask +row.price.bid)/2).toFixed(4)}</div>,
  },
  {
	name:'Trend',
	cell: row => <div><MyButton className={((row.price.ask +row.price.bid)/2) > row.oldprice ? 'up' : 'down'}  text={((row.price.ask +row.price.bid)/2).toFixed(4)} /></div>,
  }
];


class Price extends React.Component {
  constructor(props) {
    super(props);
	this.state = {
		data: [],
		previousdata:[]};
  }
  
  
  
  initDataState(msg){
	 msg.map(element => {
        element.oldprice = 0; 
		return 0;
	 }		
	 );
	 
	  this.setState({data:msg});
  }
  
  updateDataState(msg){
	if(this.state.data.length===0){
		this.initDataState(msg);
	}else{
        msg.map(out => { 
		out.oldprice=this.updateOldPrice(out.contract,out.exchange,out.price.bid,out.price.ask)
		return 0;
		});
		this.setState({data:msg});
    }	
	  
  }
  
  updateOldPrice(contract,exchange,bid,ask){
	  let tempdata= this.state.data;
	  let oldprice=0;
	  tempdata.map(
	  element => {
		  if(element.contract=== contract && element.exchange===exchange) { 
			oldprice = (element.price.bid+element.price.ask)/2;			
			}
			return 0;
			}
	  
	  );
	  
	  return oldprice;
	  
  }
  
  
  
  
 
  sendMessage = (msg) => {
    this.clientRef.sendMessage('/topics/all', msg);
  }
 
  render() {
    return (
      <div>
        <SockJsClient url='http://localhost:8080/products' topics={['/topic/pricelist']}
            onMessage={(msg) => { 
				this.updateDataState(msg);
			}}
            ref={ (client) => { this.clientRef = client }} />
		 <DataTable 
			title="Stock Price to Trade"
			columns={columns}
			data={this.state.data}
			/>
      </div>
    );
  }
}
  export default Price;