import React from 'react';
import './Button.css'

  class MyButton extends React.Component {
	  constructor(props) {
		super(props);
	}

      render() {
	    return (<button className={this.props.className}>{this.props.text}</button>);
      }
    
  }

  export default MyButton;