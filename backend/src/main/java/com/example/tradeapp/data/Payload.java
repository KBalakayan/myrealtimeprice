package com.example.tradeapp.data;

import java.util.List;

public class Payload {
	
	private List<Product> msg;

	public List<Product> getMsg() {
		return msg;
	}

	public void setMsg(List<Product> msg) {
		this.msg = msg;
	}
	
  
}
