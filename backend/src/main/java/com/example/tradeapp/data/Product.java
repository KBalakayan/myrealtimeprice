package com.example.tradeapp.data;

public class Product {
	
	private String name;
	private String exchange;
	private String contract;
	private Price price;
	
	public Product() {
		
	}
	
	public Product(String name,String exchange,String contract,Price price) {
		this.name=name;
		this.exchange=exchange;
		this.contract=contract;
		this.price=price;		
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public Price getPrice() {
		return price;
	}
	public void setPrice(Price price) {
		this.price = price;
	}

}
