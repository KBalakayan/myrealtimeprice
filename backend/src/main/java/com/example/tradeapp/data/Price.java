package com.example.tradeapp.data;

import java.time.LocalDateTime;
import java.util.Date;

public class Price {
	
	private double bid=0.0;
	private double ask=0.0;
	private LocalDateTime  datetime;
	
	
	public Price(double bid, double ask,LocalDateTime timestamp) {
      this.ask=ask;
      this.bid=bid;
      this.datetime=timestamp;
	}

	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	public double getAsk() {
		return ask;
	}

	public void setAsk(double ask) {
		this.ask = ask;
	}
	
	public LocalDateTime getDatetime() {
		return datetime;
	}
	
	public void setDatetime(LocalDateTime datetime) {
		this.datetime = datetime;
	}

}
