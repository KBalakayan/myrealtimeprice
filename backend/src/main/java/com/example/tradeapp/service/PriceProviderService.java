package com.example.tradeapp.service;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.example.tradeapp.data.Price;

@Service
public class PriceProviderService {

	
	//dummy price provider
	public Price getPrice(String contract, String exchange) {
		double bid=0.0;
		double ask=0.0;
		double last=0.0;
		
		if(contract.equals("D05") && exchange.equals("SGX")) {
			last=21.50;
		}
		if(contract.equals("Z74") && exchange.equals("SGX")) {
			last=2.53;
		}
		if(contract.equals("S63") && exchange.equals("SGX")) {
			last=3.37;
		}   
		int randomValue = (int)(Math.random( ) * 99);

		bid=last+randomValue*0.01;
		bid*=1.01;
		ask=last+randomValue*0.01;	
		ask*=0.99;
		LocalDateTime now = LocalDateTime.now(); 
		Price price =new Price(bid,ask,now);
		
		return price;
	}


	
}
