package com.example.tradeapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.tradeapp.data.Payload;
import com.example.tradeapp.data.Price;
import com.example.tradeapp.data.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
@Service
public class ProductService {
	

	   @Autowired
	   PriceProviderService priceProviderService;
	
	    // dummy productlist
	
		public List<Product> getAllowedProducts(){
			
			List<Product> listOfProducts = new ArrayList<Product>();
			
			listOfProducts.add(new Product("DBS Group Holdings Ltd","SGX","D05",priceProviderService.getPrice("D05","SGX")) );
			listOfProducts.add(new Product("Singapore Telecommunications Limited","SGX","Z74",priceProviderService.getPrice("Z74","SGX")) );
			listOfProducts.add(new Product("Singapore Technologies Engineering Ltd","SGX","S63",priceProviderService.getPrice("S63","SGX")) );
				
			return listOfProducts;
			
		}
	
	    @Autowired
	    private SimpMessagingTemplate template;
	   

	    @Scheduled(fixedDelay=5000)
	    public void livePrice() throws JsonProcessingException{
	     	
	        template.convertAndSend("/topic/pricelist", getAllowedProducts());
	    }
	
	
	
}
